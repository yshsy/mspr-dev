package mspr.dev.parsers;
import mspr.dev.models.Equipement;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;


public class ParseMateriel {
    public static List<Equipement> get(String path) throws IOException {
        // On obtient le chemin du fichier liste.txt
        FileSystem fs = FileSystems.getDefault();
        Path materielPath = fs.getPath(path+"/liste.txt");

        // On obtient le nombre de lignes dans le fichier
        int nbLignes = Math.toIntExact(Files.lines(materielPath).count());

        // On exporte son contenu dans un ArrayList
        List<String> data = Files.readAllLines(materielPath);

        // On crée un tableau 2D
        String[][] listeMateriel = new String[nbLignes][2];

        List<Equipement> equipements = new ArrayList<>();

        // Export des données dans l'Arraylist d'équipements
        for(int i = 0; i < nbLignes; i++){
            String[] donnees = data.get(i).split("\t");
            Equipement eq = new Equipement(donnees[0],donnees[1]);
            equipements.add(eq);
        }

        return equipements;
    }
}