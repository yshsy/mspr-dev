package mspr.dev.parsers;
import mspr.dev.models.Agent;
import mspr.dev.models.Equipement;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class ParseStaff {
    public static List<Agent> get(String path, String webAlias, List<Equipement> materiel) throws IOException {
        // On récupère le chemin du fichier staff.txt
        FileSystem fs = FileSystems.getDefault();
        Path staffPath = fs.getPath(path + "/staff.txt");

        // On exporte son contenu dans un ArrayList
        List<String> data = Files.readAllLines(staffPath);

        // On crée une ArrayList d'agents
        List<Agent> agents = new ArrayList<>();

        // Pour chaque agent
        for (String a : data) {
            // On exporte chaque informations d'agent dans une liste de Strings
            Path agentPath = fs.getPath(path + "/agents/" + a + ".txt");
            Path idPath = fs.getPath(webAlias + "/agents/" + a + ".jpg");
            List<String> agentData = Files.readAllLines(agentPath);

            // Récupération des noms et des descriptions de chaque équipement de l'agent
            List<Equipement> equipements = new ArrayList<>();
            for (int i = 4; i < agentData.size(); i++) {
                for (Equipement e : materiel) {
                    if (Objects.equals(agentData.get(i), e.getNom())) {
                        Equipement eq = new Equipement(e.getNom(), e.getDesc());
                        equipements.add(eq);
                    }
                }
            }
            Agent ag = new Agent(a, agentData.get(0), agentData.get(1), agentData.get(2), idPath, equipements);
            agents.add(ag);
        }

        return agents;
    }
}