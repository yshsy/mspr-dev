package mspr.dev.models;

public class Equipement {
    String nom, desc;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Equipement(String nom, String desc) {
        this.nom = nom;
        this.desc = desc;
    }
}
