package mspr.dev.models;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class Agent {
    String pseudo, nom, prenom, role;
    Path idPath;
    List<Equipement> equipements = new ArrayList<>();

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
    public Path getIdPath() {
        return idPath;
    }

    public void setIdPath(Path role) {
        this.idPath = idPath;
    }


    public List<Equipement> getEquipements() {
        return equipements;
    }

    public void setEquipements(List<Equipement> equipements) {
        this.equipements = equipements;
    }

    public Agent(String pseudo, String nom, String prenom, String role, Path idPath, List<Equipement> equipements) {
        this.pseudo = pseudo;
        this.nom = nom;
        this.prenom = prenom;
        this.role = role;
        this.idPath = idPath;
        this.equipements = equipements;
    }
}
