package mspr.dev;
import mspr.dev.*;
import mspr.dev.parsers.*;
import mspr.dev.models.*;

import java.io.IOException;
import java.util.List;

public class App
{
    public static void main(String[] args) throws IOException {
        // Attribution paramètres <> variables
        String filesPath = args[0];
        String webPath = args[1];
        String webAlias = args [2];

        // Obtention du matériel
        List<Equipement> materiel = ParseMateriel.get(filesPath);

        // Obtention des agents
        List<Agent> agents = ParseStaff.get(filesPath, webAlias, materiel);

        // Génération des fiches agent HTML
        WebGenerateur.generate(agents, filesPath, webPath, webAlias);
    }
}