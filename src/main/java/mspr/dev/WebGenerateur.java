package mspr.dev;

import mspr.dev.models.Agent;
import mspr.dev.models.Equipement;

import java.io.*;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;

public class WebGenerateur {

    public static void generate(List<Agent> agents, String filesPath, String webPath, String webAlias) throws IOException {
        // On récupère le chemin du modèle index
        FileSystem fs = FileSystems.getDefault();
        Path modeleIndexPath = fs.getPath(filesPath+"/modeles/modeleIndex.html");

        // On exporte son contenu dans un ArrayList
        List<String> indexData = Files.readAllLines(modeleIndexPath);
        // On prépare un ArrayList pour le contenu final
        List<String> indexHTML = new ArrayList<>();

        // On exporte le modèle de la card agent dans un ArrayList
        Path modeleCardIndexPath = fs.getPath(filesPath+"/modeles/modeleCardIndex.html");
        List<String> cardIndexData = Files.readAllLines(modeleCardIndexPath);

        // On copie le logo GO Securi


        // Génération - Index
        int i = 0;
        for (String l : indexData) {
            // Chemin dossier données
            if (l.contains("{{filesPath}}")) {
                l = l.replace("{{filesPath}}", webAlias);
            }

            // Génération card agents
            if (l.contains("{{listeAgents}}")) {
                for (Agent a : agents) {
                    for (String m : cardIndexData) {
                        // Chemin image carte identité
                        if (m.contains("{{idPath}}")) {
                            m = m.replace("{{idPath}}", a.getIdPath().toString());
                        }
                        // Nom agent
                        if (m.contains("{{agentNom}}")) {
                            m = m.replace("{{agentNom}}", a.getNom() + " " + a.getPrenom());
                        }
                        // Chemin fiche agent
                        if (m.contains("{{agentPath}}")) {
                            m = m.replace("{{agentPath}}", a.getPseudo() + ".html");
                        }
                        indexHTML.add(i,m);
                        i++;
                    }
                }
            }
            indexHTML.add(i,l);
            i++;
        }
        // Ecriture du fichier final dans index.html
        FileWriter indexWriter = new FileWriter(webPath+"/index.html");
        for (String l : indexHTML) {
            indexWriter.write(l);
        }
        indexWriter.close();


        // Génération - Fiches Agent
        // On récupère le chemin du modèle index
        Path modeleAgentPath = fs.getPath(filesPath+"/modeles/modeleAgent.html");

        for (Agent a : agents) {
            // On exporte son contenu dans un ArrayList
            List<String> agentData = Files.readAllLines(modeleAgentPath);
            // On prépare un ArrayList pour le contenu final
            List<String> agentHTML = new ArrayList<>();

            int j = 0;
            for (String l : agentData) {
                // Nom agent
                if (l.contains("{{agentNom}}")) {
                    l = l.replace("{{agentNom}}", a.getNom() + " " + a.getPrenom());
                }
                if (l.contains("{{pseudo}}")) {
                    l = l.replace("{{pseudo}}", a.getPseudo());
                }
                // Chemin dossier données
                if (l.contains("{{filesPath}}")) {
                    l = l.replace("{{filesPath}}", webAlias);
                }
                // Génération liste équipements
                if (l.contains("{{listeEquipements}}")) {
                    for (Equipement e : a.getEquipements()) {
                        String k = "<li class=\"list-group-item\">"+e.getDesc()+"</li>";
                        agentHTML.add(j, k);
                        j++;
                    }
                }
                agentHTML.add(j,l);
                j++;
            }

            // Ecriture du fichier final dans {{agent}}.html
            FileWriter agentWriter = new FileWriter(webPath+"/"+a.getPseudo()+".html");
            for (String l : agentHTML) {
                agentWriter.write(l);
            }
            agentWriter.close();
        }
    }
}
